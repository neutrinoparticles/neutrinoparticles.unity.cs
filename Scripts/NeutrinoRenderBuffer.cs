﻿//#define CACHE_TESTING
using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace NeutrinoParticles.Unity3D
{
	public class NeutrinoRenderBuffer : NeutrinoParticles.RenderBuffer
	{
        struct SubMesh
        {
            public int renderStyleIndex;
            public List<int> indices;
        }

		#region Fields
		private Mesh mesh_;

		private List<Vector3> vertices_ = new List<Vector3>();
		private List<Vector2> uv_ = new List<Vector2>();
		private List<Color> colors_ = new List<Color>();
        private List<SubMesh> subMeshes_ = new List<SubMesh>(1);
        private int numSubMeshes_ = 0;

#if UNITY_EDITOR
		private Bounds bounds_ = new Bounds();
#endif
		#endregion

		#region Properties

		public Mesh Mesh
		{
			get
			{
				return mesh_;
			}
		}

		#endregion

		#region Methods

		public NeutrinoRenderBuffer(Mesh mesh)
		{
			mesh_ = mesh;
			mesh_.MarkDynamic();
		}

        public int subMeshRenderStyleIndex(int subMeshIndex)
        {
            return subMeshes_[subMeshIndex].renderStyleIndex;
        }

		public void initialize(uint maxNumVertices, uint[] texChannels, uint maxNumRenderCalls)
		{
		}

		public void cleanup()
		{
			vertices_.Clear();
			colors_.Clear();
			uv_.Clear();

            foreach (var subMesh in subMeshes_)
            {
                subMesh.indices.Clear();
            }

            numSubMeshes_ = 0;
        }

        public void beginRenderCall(ushort renderStyleIndex)
        {
            if (numSubMeshes_ == 0 || 
                subMeshes_[numSubMeshes_ - 1].renderStyleIndex != renderStyleIndex)
            {
                if (subMeshes_.Count < numSubMeshes_ + 1)
                {
                    SubMesh subMesh;
                    subMesh.renderStyleIndex = renderStyleIndex;
                    subMesh.indices = new List<int>(6);
                    subMeshes_.Add(subMesh);
                }
                else
                {
                    SubMesh subMesh = subMeshes_[numSubMeshes_];
                    subMesh.renderStyleIndex = renderStyleIndex;
                    subMeshes_[numSubMeshes_] = subMesh;
                }

                ++numSubMeshes_;
            }
        }

        public void pushGeometry(RenderVertex[] vertices, ushort[] indices)
        {
            int startVertex = vertices_.Count;

            foreach (var v in vertices)
            {
                Vector3 v3;
                v3.x = v.position_.x;
                v3.y = v.position_.y;
                v3.z = v.position_.z;
                vertices_.Add(v3);

                Vector2 v2;
                v2.x = v.texCoords_[0][0];
                v2.y = v.texCoords_[0][1];
                uv_.Add(v2);

                Color c;
                c.a = ((float)((v.color_ & 0xff000000) >> 24)) / 255f;
                c.r = ((float)(v.color_ & 0x000000ff)) / 255f;
                c.g = ((float)((v.color_ & 0x0000ff00) >> 8)) / 255f;
                c.b = ((float)((v.color_ & 0x00ff0000) >> 16)) / 255f;
                colors_.Add(c);
            }

            var subMesh = subMeshes_[numSubMeshes_ - 1];

            foreach (var index in indices)
            {
                subMesh.indices.Add(index + startVertex);
            }
        }

        public void endRenderCall(ushort renderStyleIndex)
        {
        }

		public void updateMesh()
		{
			if (vertices_.Count == 0)
				return;

			mesh_.Clear(true);

			mesh_.SetVertices(vertices_);
			mesh_.SetColors(colors_);
			mesh_.SetUVs(0, uv_);

			mesh_.subMeshCount = numSubMeshes_;

			for (int subMeshIndex = 0; subMeshIndex < numSubMeshes_; ++subMeshIndex)
			{
				mesh_.SetTriangles(subMeshes_[subMeshIndex].indices, subMeshIndex);
			}


#if UNITY_EDITOR
			if (mesh_.bounds.center != Vector3.zero)
			{
				bounds_.SetMinMax(mesh_.bounds.min, mesh_.bounds.max);
				bounds_.center = Vector3.zero;
				mesh_.bounds = bounds_;
			}
#endif
		}

        #endregion
    }
}